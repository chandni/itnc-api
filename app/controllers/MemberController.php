<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MemberController extends Controller {

    function authenticate() {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $ITSId = $this->f3->get('POST.ITSId');
            $password = $this->f3->get('POST.Password');
            $member = new Member($this->db);
            $memberId = $member->validateLogin($ITSId, $password);
            $securitySalt = $this->f3->get('Security.salt');
            $authToken = $this->generateRandomString(25);
            $authTokenMD5 = md5($securitySalt . $authToken);
            //update auth_token in Member table
            $update = $member->updateToken($authTokenMD5, $memberId);
            if ($update) {
                $response = array("auth_token" => $authTokenMD5, "memberid" => $memberId);
                $this->responseObj->setResponseData($response);
            } else {
                $this->responseObj->setData('0', '401', "Username OR Password Incorrect");
            }
        } else {
            $errorMessage = array('message' => "Request is not post");
            $this->responseObj->setData('0', '303', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

    /**
     * Generate random string method.
     *
     * Use this method to generate random string.
     *
     * @return randomString
     */
    function generateRandomString($length) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $randomString = substr(str_shuffle($chars), 0, $length);
        return $randomString;
    }

    function getall() {
        if ($this->isAppUserAuthorized()) {
            $member = new Member($this->db);
            $result = $member->getAllMembers();
            if (!empty($result)) {
                $this->responseObj->setResponseData($result);
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

    function getMemberById() {
        if ($this->isAppUserAuthorized()) {
            $id = $this->f3->get('PARAMS.id');
            $member = new Member($this->db);
            $result = $member->getMemberById($id);
            if (!empty($result)) {
                $this->responseObj->setResponseData($result);
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

    function getMemberPosts() {
        if ($this->isAppUserAuthorized()) {
            $id = $this->f3->get('PARAMS.id');
            $member = new Member($this->db);
            $result = $member->getMemberPosts($id);
            if (!empty($result)) {
                $this->responseObj->setResponseData($result);
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

    function getMemberAlerts() {
        if ($this->isAppUserAuthorized()) {
            $id = $this->f3->get('PARAMS.id');
            $member = new Member($this->db);
            $result = $member->getMemberAlerts($id);
            if (!empty($result)) {
                $this->responseObj->setResponseData($result);
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

    function getMemberCandidates() {
        if ($this->isAppUserAuthorized()) {
            $id = $this->f3->get('PARAMS.id');
            $member = new Member($this->db);
            $result = $member->getMemberCandidates($id);
            if (!empty($result)) {
                $this->responseObj->setResponseData($result);
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

    function checkKey() {
        if ($this->isAppUserAuthorized()) {
            $memberid = $this->f3->get('POST.memberid');
            $requestTncKey = $this->f3->get('POST.tnckey');
            $member = new Member($this->db);
            $TNCKeys = $member->checkKey($memberid);
            if (!empty($TNCKeys)) {
                $result = 'false';
                foreach ($TNCKeys as $tnckey) {
                    if ($tnckey['TNCKey'] == $requestTncKey) {
                        $result = 'true';
                        break;
                    }
                }
                $this->responseObj->setResponseData($result);
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

}
