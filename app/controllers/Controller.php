<?php

/**
 * Generic Parent Controller of the Sample Site
 * All controllers extend this class
 *
 * PHP version 5
 * 
 * @category PHP
 * @package  Fat-Free-PHP-Bootstrap-Site
 * @author   Mark Takacs <takacsmark@takacsmark.com>
 * @license  MIT 
 * @link     takacsmark.com
 */

/**
 * Parent Controller class
 * 
 * @category PHP
 * @package  Fat-Free-PHP-Bootstrap-Site
 * @author   Mark Takacs <takacsmark@takacsmark.com>
 * @license  MIT 
 * @link     takacsmark.com
 */
class Controller {

    protected $f3;
    protected $db;
    protected $responseObj;
    protected $web;
    protected $log;

    /**
     * The beforeroute event handler is provided by f3 and it is 
     * automatically invoked by f3 before every time routing happens
     *
     * We check in the below code if there is a user information in the session 
     * i.e. if there is a logged in user 
     *
     * @return void 
     */
    function beforeroute() {
//        if($this->f3->get('SESSION.user') === null ) {
//            $this->f3->reroute('/login');
//            exit;
//        }
    }

    public function isAppUserAuthorized() {
        $requestHeader = apache_request_headers();
        $this->log->write(json_encode($requestHeader));
        $headerToken = isset($requestHeader["Auth-Token"]) ? $requestHeader["Auth-Token"] : $requestHeader["auth_token"];
        $this->log->write($headerToken);
        if ($headerToken) {
            if ($headerToken) {
                $member = new Member($this->db);
                $result = $member->isAppUserAuthorized($headerToken);
                if ($result) {
                    $this->setAppUserSession($result);
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        } else {
            $errorMessage = array('message' => "Invalid token");
            $this->responseObj->setData('0', '301', $errorMessage);
            echo $this->responseObj->getJson();
        }
    }

    public function setAppUserSession($validateMember) {
        $this->f3->set('Auth', $validateMember);
        $this->f3->clear('Auth.User.password');
        $this->f3->write('Auth.Member', $validateMember['Member']);

//        $this->setSessionVariables();
    }

    /**
     * setSessionVariables
     *
     * It will set session variables for app
     *
     */
    public function setSessionVariables() {
        $this->session_user_id = $this->Session->read("Auth.User.id");
        $this->session_user_full_name = $this->Session->read("Auth.User.full_name");
    }

    /**
     * The beforeroute event handler is provided by f3 and it is 
     * automatically invoked by f3 after every time routing happens
     *
     * The below code is just a placeholder 
     *
     * @return void 
     */
    function afterroute() {
        // your code comes here
    }

    /**
     * Class constructor 
     * We connect to the mysql database here and  
     * Assign value to f3 and db protected class variables defined above
     *
     * @return void 
     */
    function __construct() {
        $this->responseObj = new CResponse();
        $this->log = new Log('error.log');
        $f3 = Base::instance();
        $web = Web::instance();
        $this->f3 = $f3;
        $this->web = $web;

        $db = new \DB\SQL(
                $f3->get('devdb'), $f3->get('devdbusername'), $f3->get('devdbpassword'), array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION)
        );

        $this->db = $db;
    }

}
