<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LookupController extends Controller {

    function getTable() {
        if ($this->isAppUserAuthorized()) {
            $table = $this->f3->get('PARAMS.table');
            if ($table == 'jamaat') {
                $result = $this->getJamaatQuery();
            } else if ($table == 'country') {
                $result = $this->getCountryQuery();
            } else if ($table == 'place') {
                $result = $this->getPlaceQuery();
            }
            if (!empty($result)) {
                $this->responseObj->setResponseData($result);
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

    function getJamaatQuery() {
        $jamaat = new Jamaat($this->db);
        $result = $jamaat->getJamaat();
        return $result;
    }

    function getCountryQuery() {
        $country = new Country($this->db);
        $result = $country->getCountry();
        return $result;
    }

    function getPlaceQuery() {
        $place = new Place($this->db);
        $result = $place->getPlaces();
        return $result;
    }

    function getAnnouncements() {
        if ($this->isAppUserAuthorized()) {
            $place = new Announcement($this->db);
            $result = $place->getAnnouncements();
            if (!empty($result)) {
                $this->responseObj->setResponseData($result);
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

}
