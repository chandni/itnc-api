<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CandidateController extends Controller {

    function getall() {
        if ($this->isAppUserAuthorized()) {
            $candidate = new Candidate($this->db);
            $result = $candidate->getAllCandidates();
            if (!empty($result)) {
                $this->responseObj->setResponseData($result);
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

    function getCandidateById() {
        if ($this->isAppUserAuthorized()) {
            $id = $this->f3->get('PARAMS.id');
            $candidate = new Candidate($this->db);
            $result = $candidate->getCandidateById($id);
            if (!empty($result)) {
                $this->responseObj->setResponseData($result);
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

    function getCandidateProfile() {
        if ($this->isAppUserAuthorized()) {
            $id = $this->f3->get('PARAMS.id');
            $candidate = new Candidate($this->db);
            $result = $candidate->getCandidateProfile($id);
            if (!empty($result)) {
                $this->responseObj->setResponseData($result);
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

    function getCandidateShorlist() {
        if ($this->isAppUserAuthorized()) {
            $id = $this->f3->get('PARAMS.id');
            $candidate = new Candidate($this->db);
            $result = $candidate->getCandidateShortList($id);
            if (!empty($result)) {
                $this->responseObj->setResponseData($result);
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

    function getCandidateRemarks() {
        if ($this->isAppUserAuthorized()) {
            $id = $this->f3->get('PARAMS.id');
            $bMessage = False;
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $remarks = $this->f3->get('POST.remarks');
                $remarkid = $this->f3->get('POST.remarkid');
                $result = $this->postQuery($remarkid, $remarks, $id);
                $bMessage = TRUE;
                $message = "Remark Updated Successfully";
            } else if ($_SERVER['REQUEST_METHOD'] == 'GET') {
                $result = $this->getQuery($id);
            } else if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
                parse_str(file_get_contents("php://input"), $post_vars);
                $remarks = $post_vars['remarks'];
                $result = $this->putQuery($id, $remarks);
                $bMessage = TRUE;
                $message = "Remark Inserted Successfully";
            }
//            $result = $this->db->exec($query);
            if (!empty($result)) {
                if ($bMessage) {
                    $this->responseObj->setResponseData($message);
                } else {
                    $this->responseObj->setResponseData($result);
                }
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

    function getQuery($canidateId) {
        $candidate = new Candidate($this->db);
        return $remarks = $candidate->getRemarks($canidateId);
    }

    function postQuery($remarkId, $remarks, $canidateId) {
        $candidate = new Candidate($this->db);
        return $updateRemarks = $candidate->updateRemarks($remarkId, $remarks, $canidateId);
    }

    function putQuery($id, $remarks) {
        $candidate = new Candidate($this->db);
        return $insertRemarks = $candidate->insertRemarks($id, $remarks);
    }

    function deleteCandidateRemarks() {
        if ($this->isAppUserAuthorized()) {
            $id = $this->f3->get('PARAMS.id');
            $remarkid = $this->f3->get('PARAMS.remarkid');
            $result = $this->deleteQuery($id, $remarkid);
            if (!empty($result)) {
                $this->responseObj->setResponseData("Remark Deleted Successfully");
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

    function deleteQuery($canidateId, $candidatePrivateNodeId) {
        $candidate = new Candidate($this->db);
        return $result = $candidate->deleteRemarks($candidatePrivateNodeId, $canidateId);
    }

}
