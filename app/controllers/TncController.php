<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class TncController extends Controller {

    function getall() {
        if ($this->isAppUserAuthorized()) {
            $tnc = new TNC($this->db);
            $result = $tnc->getAllTNC();
            if (!empty($result)) {
                $this->responseObj->setResponseData($result);
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

    function getTncById() {
        if ($this->isAppUserAuthorized()) {
            $id = $this->f3->get('PARAMS.id');
            $tnc = new TNC($this->db);
            $result = $tnc->getTNCById($id);
            if (!empty($result)) {
                $this->responseObj->setResponseData($result);
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

    function getTncStatistics() {
        
    }

    function getTncAnalytics() {
        
    }

    function getTncMembers() {
        if ($this->isAppUserAuthorized()) {
            $id = $this->f3->get('PARAMS.id');
            $tnc = new TNC($this->db);
            $result = $tnc->getTNCMembers($id);
            if (!empty($result)) {
                $this->responseObj->setResponseData($result);
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

    function getTncCandidates() {
        if ($this->isAppUserAuthorized()) {
            $id = $this->f3->get('PARAMS.id');
            $tnc = new TNC($this->db);
            $result = $tnc->getTNCCandidates($id);
            if (!empty($result)) {
                $this->responseObj->setResponseData($result);
            } else {
                $this->responseObj->setData('0', '401', "No data found");
            }
        } else {
            $errorMessage = array('message' => "User is not authorized");
            $this->responseObj->setData('0', '401', $errorMessage);
        }
        echo $this->responseObj->getJson();
    }

    function getTncSearch() {
        
    }

}
