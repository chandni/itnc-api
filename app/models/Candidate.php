<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Candidate extends DB\SQL\Mapper {

    /**
     * Constructor, maps user table fields to php object
     * 
     * @param DB\SQL $db Database connection
     */
    public function __construct(DB\SQL $db) {
        parent::__construct($db, 'CandidateEx');
    }

    public function getAllCandidates() {
        $result = $this->db->exec(" SELECT
        ce.CandidateId,
        ce.ITSId,
        ce.FullName
    FROM
        CandidateEx ce
    ORDER BY
        ce.FullName
    LIMIT 10
   ");
        return $result;
    }

    public function getCandidateById($id) {
        $result = $this->db->exec(" SELECT
        ce.ITSId,
        ce.FullName
    FROM
        CandidateEx ce
    WHERE
        ce.CandidateId = $id
    LIMIT 1
    ");
        return $result;
    }

    public function getCandidateProfile($id) {
        $result = $this->db->exec(" SELECT
        ce.*,
        cfp.*
    FROM
        CandidateEx ce
        LEFT JOIN CandidateFullProfile cfp USING (CandidateId)
    WHERE
        ce.CandidateId = $id
    LIMIT 1
");
        return $result;
    }

    public function getCandidateShortList($id) {
        $result = $this->db->exec("SELECT
    ce.ITSId,
    ce.FullName,
    ce.Age,
    ce.Location,
    ce.CandidateStatusId
FROM
    (SELECT
        cm.BoyCandidateId AS CandidateId,
        cm.GirlCandidateId AS OppositeCandidateId
    FROM
        CandidateMatchmaking cm
    WHERE
        cm.BoyPairStatusId IN (2,3)
    UNION SELECT
        cm.GirlCandidateId,
        cm.BoyCandidateId
    FROM
        CandidateMatchmaking cm
    WHERE
        cm.GirlPairStatusId IN (2,3)
    ) cm
    INNER JOIN CandidateEx ce ON ce.CandidateId = cm.OppositeCandidateId
WHERE
    cm.CandidateId = $id
 ");
        return $result;
    }

    public function getRemarks($canidateId) {
        $result = $this->db->exec("SELECT
        cpn.Note, cpn.CandidatePrivateNoteId
    FROM
        CandidatePrivateNote cpn
    WHERE
        cpn.CandidateId = $canidateId
    ORDER BY
        cpn.LastUpdated DESC
");
        return $result;
    }

    public function updateRemarks($remarkId, $remarks, $canidateId) {
        $result = $this->db->exec("UPDATE
        CandidatePrivateNote cpn
    SET
        cpn.Note = '$remarks',
        cpn.LastUpdated = CURDATE()
    WHERE
        cpn.CandidatePrivateNoteId = $remarkId
        AND cpn.CandidateId = $canidateId");
        return $result;
    }

    public function insertRemarks($id, $remarks) {
        $result = $this->db->exec("INSERT INTO
        CandidatePrivateNote 
    VALUES
        (NULL,'$id','$remarks',CURDATE())");
        return $result;
    }

    public function deleteRemarks($candidatePrivateNodeId, $canidateId) {
        $result = $this->db->exec("DELETE FROM
        CandidatePrivateNote 
    WHERE
        CandidatePrivateNoteId = $candidatePrivateNodeId
        AND CandidateId = $canidateId");
        return $result;
    }

}
