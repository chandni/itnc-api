<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * TNC model
 */
class TNC extends DB\SQL\Mapper {

    /**
     * Constructor, maps user table fields to php object
     * 
     * @param DB\SQL $db Database connection
     */
    public function __construct(DB\SQL $db) {
        parent::__construct($db, 'TNC');
    }

    public function getAllTNC() {
        $result = $this->db->exec(" SELECT
        t.TNCId,
        t.TNCName
    FROM
        TNC t
    ORDER BY
        t.TNCName
    LIMIT 10
            ");
        return $result;
    }

    public function getTNCById($id) {
        $result = $this->db->exec(" SELECT
        t.TNCName
    FROM
        TNC t
    WHERE
        t.TNCId = $id
    LIMIT 1
            ");
        return $result;
    }

    public function getTNCMembers($id) {
        $result = $this->db->exec(" SELECT
        me.MemberId,
        me.ITSId,
        me.FullName,
        me.Contact,
        LOWER(me.Email) `Email`,
        GROUP_CONCAT(p.Post SEPARATOR ' & ') `Posts`
    FROM
        MemberEx me
        INNER JOIN TNCMember tm USING (MemberId)
        INNER JOIN _Post p USING (PostId)
    WHERE
        tm.TNCId = $id
    GROUP BY
        tm.MemberId
    ORDER BY
        p.PostId
 LIMIT 10");
        return $result;
    }

    public function getTNCCandidates($id) {
        $result = $this->db->exec("  SELECT
        ce.CandidateId,
        ce.ITSId,
        ce.FullName
    FROM
        CandidateEx ce
        INNER JOIN CandidateFullProfile cfp USING (CandidateId)
    WHERE
        cfp.TNCId = $id
    ORDER BY
        ce.FullName
 LIMIT 10");
        return $result;
    }

}
