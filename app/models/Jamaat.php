<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Member model
 */
class Jamaat extends DB\SQL\Mapper {

    /**
     * Constructor, maps user table fields to php object
     * 
     * @param DB\SQL $db Database connection
     */
    public function __construct(DB\SQL $db) {
        parent::__construct($db, '_Jamaat');
    }
    
    public function getJamaat(){
       $result = $this->db->exec("SELECT
        j.JamaatId,
        j.Jamaat
    FROM
        _Jamaat j
    WHERE
        j.IsSpecial = FALSE
    ORDER BY 2 LIMIT 10");
        return $result;
    }

}
