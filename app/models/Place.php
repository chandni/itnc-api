<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Place model
 */
class Place extends DB\SQL\Mapper {

    /**
     * Constructor, maps user table fields to php object
     * 
     * @param DB\SQL $db Database connection
     */
    public function __construct(DB\SQL $db) {
        parent::__construct($db, '_Place');
    }
    
    public function getPlaces(){
       $result = $this->db->exec("SELECT
        p.PlaceId,
        p.Place
    FROM
        _Place p
    ORDER BY 2 LIMIT 10");
        return $result;
    }

}
