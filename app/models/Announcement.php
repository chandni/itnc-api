<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Place model
 */
class Announcement extends DB\SQL\Mapper {

    /**
     * Constructor, maps user table fields to php object
     * 
     * @param DB\SQL $db Database connection
     */
    public function __construct(DB\SQL $db) {
        parent::__construct($db, '_Announcement');
    }
    
    public function getAnnouncements(){
       $result = $this->db->exec("SELECT
        a.AnnouncementId,
        a.Announcement
    FROM
        _Announcement a
    WHERE
        a.DisplayUpto >= CURDATE()
    ORDER BY 1 LIMIT 10");
        return $result;
    }

}
