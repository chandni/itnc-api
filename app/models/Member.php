<?php

/**
 * Member model
 */
class Member extends DB\SQL\Mapper {

    /**
     * Constructor, maps user table fields to php object
     * 
     * @param DB\SQL $db Database connection
     */
    public function __construct(DB\SQL $db) {
        parent::__construct($db, 'Member');
    }

    /**
     * Fetch all records
     * 
     * @return array
     */
    public function all() {
        $this->load();
        return $this->query;
    }

    public function validateLogin($ITSId, $password) {
        $result = $this->db->exec(" SELECT
            m.MemberId
        FROM
            Member m
        WHERE
            m.ITSId = $ITSId
            AND m.Password = '" . $password . "'
            AND m.IsSuspended = FALSE
        LIMIT 1");

        $memberId = $result[0]['MemberId'];
        return $memberId;
    }

    public function updateToken($authTokenMD5, $memberId) {
        $update = $this->db->exec("Update Member m set m.auth_token = '" . $authTokenMD5 . "' WHERE m.MemberId = '" . $memberId . "' ");
        return $update;
    }

    public function getAllMembers() {
        $result = $this->db->exec("  SELECT
        m.MemberId,
        m.FullName
    FROM
        MemberEx m
    ORDER BY 2
    LIMIT 10");
        return $result;
    }

    public function getMemberById($id) {
        $result = $this->db->exec("  SELECT
        me.ITSId,
        me.FullName,
        me.Location
    FROM
        MemberEx me
    WHERE
        me.MemberId = $id
    LIMIT 1
");
        return $result;
    }

    public function getMemberPosts($id) {
        $result = $this->db->exec("  SELECT
        t.TNCName,
        GROUP_CONCAT(p.Post SEPARATOR ' & ') `Posts`
    FROM
        TNCMember tm
        INNER JOIN _Post p USING (PostId)
        INNER JOIN TNC t USING (TNCId)
    WHERE
        tm.MemberId = $id
    GROUP BY
        tm.MemberId,
        tm.TNCId
    ORDER BY
        t.TNCName
");
        return $result;
    }

    public function getMemberAlerts($id) {
        $result = $this->db->exec(" SELECT
    FROM
    WHERE");

        return $result;
    }

    public function getMemberCandidates($id) {
        $result = $this->db->exec(" SELECT
        c1.CandidateId,
        c1.ITSId,
        c1.FullName
    FROM
        CandidateEx c1
        INNER JOIN CandidateFullProfile c2 USING (CandidateId)
    WHERE
        c2.CounselorMemberId = $id");
        return $result;
    }

    public function checkKey($memberid) {
        $TNCKeys = $this->db->exec(" SELECT DISTINCT
        t.TNCKey
    FROM
        TNC t
        INNER JOIN TNCMember tm USING (TNCId)
    WHERE
        tm.MemberId = $memberid
        AND t.TNCStatusId IN (3,4,5)");
        return $TNCKeys;
    }

    public function isAppUserAuthorized($token) {
        $result = $this->db->exec(" SELECT * FROM Member m
        WHERE
            m.auth_token = '" . $token . "'
            AND m.IsSuspended = FALSE
        LIMIT 1");
        return $result;
    }

}
