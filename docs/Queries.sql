-- GET /tnc
    SELECT
        t.TNCId,
        t.TNCName
    FROM
        TNC t
    ORDER BY
        t.TNCName
    LIMIT 10

-- GET /tnc/{id}
    SELECT
        t.TNCName
    FROM
        TNC t
    WHERE
        t.TNCId = :id
    LIMIT 1

-- GET /tnc/{id}/statistics/{year}/{month}
    -- SELECT
    -- FROM
    -- WHERE

-- GET /tnc/{id}/analytics
    -- SELECT
    -- FROM
    -- WHERE

-- GET /tnc/{id}/members
    SELECT
        me.MemberId
        me.ITSId,
        me.FullName,
        me.Contact,
        LOWER(me.Email) `Email`,
        GROUP_CONCAT(p.Post SEPARATOR ' & ') `Posts`
    FROM
        MemberEx me
        INNER JOIN TNCMember tm USING (MemberId)
        INNER JOIN _Post p USING (PostId)
    WHERE
        tm.TNCId = :id
    GROUP BY
        tm.MemberId
    ORDER BY
        p.PostId

-- GET /tnc/{id}/candidates
    SELECT
        ce.CandidateId,
        ce.ITSId,
        ce.FullName
    FROM
        CandidateEx ce
        INNER JOIN CandidateFullProfile cfp USING (CandidateId)
    WHERE
        cfp.TNCId = :id
    ORDER BY
        ce.FullName

-- GET /tnc/search?{partialtncname,jamaatid,placeid,countryid}
    SELECT DISTINCT
        t.TNCId,
        t.TNCName
    FROM
        TNC t
        INNER JOIN TNCJamaat tj USING (TNCId)
        INNER JOIN _Jamaat j USING (JamaatId)
        INNER JOIN _Place p USING (PlaceId)
        INNER JOIN _Country c USING (CountryId)
    WHERE
        TRUE
        OR t.TNCName LIKE CONCAT('%',:partialtncname,'%')
        OR tj.JamaatId = :jamaatid
        OR p.PlaceId = :placeid
        OR c.CountryId = :countryid
    ORDER BY
        t.TNCName
    
-- GET /tnc/searchmember?{partialtncname,jamaatid,placeid,countryid,partialmembername,postid}
    SELECT
        me.MemberId,
        me.ITSId,
        me.FullName,
        me.Contact,
        LOWER(me.Email) `Email`,
        t.TNCName,
        GROUP_CONCAT(p1.Post ORDER BY p1.PostId SEPARATOR ' & ') `Posts`
    FROM
        MemberEx me
        INNER JOIN TNCMember tm USING (MemberId)
        INNER JOIN _Post p1 USING (PostId) -- a second post can be added here to fix the posts bug
        INNER JOIN TNC t ON t.TNCId = tm.TNCId
        INNER JOIN TNCJamaat tj ON tj.TNCId = t.TNCId
        INNER JOIN _Jamaat j ON j.JamaatId = tj.JamaatId
        INNER JOIN _Place p2 USING (PlaceId)
        INNER JOIN _Country c USING (CountryId)
    WHERE
        TRUE
        OR t.TNCName LIKE CONCAT('%',:partialtncname,'%')
        OR tj.JamaatId = :jamaatid
        OR p2.PlaceId = :placeid
        OR c.CountryId = :countryid
        OR me.FullName LIKE CONCAT('%',:partialmembername,'%')
        OR p1.PostId = :postid -- bug, if selected can't see member's other posts
    GROUP BY
        me.ITSId,
        tm.TNCId -- for those in multiple TNCs
    ORDER BY
        me.FullName

-- GET /member
    SELECT
        me.MemberId,
        me.ITSId,
        me.FullName
    FROM
        MemberEx me
    ORDER BY
        me.FullName
    LIMIT 10

-- POST /member/authenticate?{itsid,password}
    SELECT
        m.MemberId
    FROM
        Member m
    WHERE
        m.ITSId = :itsid
        AND m.Password = :password
        AND m.IsSuspended = FALSE
    LIMIT 1

-- POST /member/{id}/checkkey?{tnckey}
    SELECT
        :tnckey IN
        (SELECT DISTINCT
            t.TNCKey
        FROM
            TNC t
            INNER JOIN TNCMember tm USING (TNCId)
        WHERE
            tm.MemberId = :id
            AND t.TNCStatusId IN (3,4,5)
        ) t
        
-- GET /member/{id}
    SELECT
        me.ITSId
        me.FullName,
        me.Location
    FROM
        MemberEx me
    WHERE
        me.MemberId = :id
    LIMIT 1

-- GET /member/{id}/posts
    SELECT
        t.TNCName,
        GROUP_CONCAT(p.Post SEPARATOR ' & ') `Posts`
    FROM
        TNCMember tm
        INNER JOIN _Post p USING (PostId)
        INNER JOIN TNC t USING (TNCId)
    WHERE
        tm.MemberId = :id
    GROUP BY
        tm.MemberId,
        tm.TNCId
    ORDER BY
        t.TNCName

-- GET /member/{id}/alerts
    -- SELECT
    -- FROM
    -- WHERE

-- GET /member/{id}/candidates
    SELECT
        ce.CandidateId,
        ce.ITSId,
        ce.FullName,
        ce.Age,
        ce.Gender,
        ce.Contact,
        LOWER(ce.Email) `Email`,
        ce.CandidateStatusId
    FROM
        CandidateEx ce
        INNER JOIN CandidateFullProfile cfp USING (CandidateId)
    WHERE
        cfp.CounselorMemberId = :id

-- GET /candidate
    SELECT
        ce.CandidateId,
        ce.ITSId,
        ce.FullName
    FROM
        CandidateEx ce
    ORDER BY
        ce.FullName
    LIMIT 10

-- GET /candidate/{id}
    SELECT
        ce.ITSId,
        ce.FullName
    FROM
        CandidateEx ce
    WHERE
        ce.CandidateId = :id
    LIMIT 1

-- GET /candidate/{id}/profile
-- with all lookups
    SELECT
        ce.ITSId,
        ce.FullName,
        ce.Gender,
        ce.MaritalStatus,
        ce.DateOfBirth,
        ce.Age,
        cfp.HeightInInches,
        cfp.NumberOfChildren,
        j.Jamaat,
        TRIM(CONCAT_WS(' ',fnf.Prefix,cfp.FathersFirstName,fnf.Suffix,cfp.FathersLastName)) FathersFullName,
        TRIM(CONCAT_WS(' ',mnf.Prefix,cfp.MothersFirstName,mnf.Suffix,cfp.MothersLastName)) MothersFullName,
        cfp.`Group`,
        cfp.Address1,
        cfp.Address2,
        p.Place,
        pc.Country,
        cfp.PinZipCode,
        rs.ResidentialStatus,
        cc.Country,
        cfp.Mobile,
        cfp.Landline,
        LOWER(cfp.Email) `Email`,
        cfp.Watan,
        wr.WatanRegion,
        hq.HifzUlQuraan,
        al.AlJameaLevel,
        cfp.IsStillInJamea,
        cel.EducationLevel CurrentEducationLevel,
        pel.EducationLevel PursuingEducationLevel,
        cfp.PursuingYear,
        cfp.EducationDetails,
        oc.OccupationCategory,
        o.Occupation,
        cfp.OccupationDetails,
        cfp.VoluntaryKhidmatDetails,
        cfp.AboutSelf,
        i1.Interest Interest1,
        i2.Interest Interest2,
        i3.Interest Interest3,
        cfp.SpousePreferences,
        cfp.ParentsViews,
        rb.RegisteredBy
    FROM
        CandidateEx ce
        LEFT JOIN CandidateFullProfile cfp ON cfp.CandidateId = ce.CandidateId
        INNER JOIN _Jamaat j USING (JamaatId)
        INNER JOIN _NameFix fnf ON fnf.NameFixId = cfp.FathersNameFixId
        INNER JOIN _NameFix mnf ON mnf.NameFixId = cfp.MothersNameFixId
        INNER JOIN _Place p ON p.PlaceId = cfp.PlaceId
        INNER JOIN _Country pc ON pc.CountryId = p.CountryId
        INNER JOIN _ResidentialStatus rs USING (ResidentialStatusId)
        INNER JOIN _Country cc ON cc.CountryId = cfp.CitizenOfCountryId
        INNER JOIN _HifzUlQuraan hq USING (HifzUlQuraanId)
        INNER JOIN _AlJameaLevel al USING (AlJameaLevelId)
        INNER JOIN _EducationLevel cel ON cel.EducationLevelId = cfp.CompletedEducationLevelId
        LEFT JOIN _EducationLevel pel ON pel.EducationLevelId = cfp.PursuingEducationLevelId
        INNER JOIN _Occupation o USING (OccupationId)
        INNER JOIN _OccupationCategory oc USING (OccupationCategoryId)
        INNER JOIN _Interest i1 ON i1.InterestId = cfp.Interest1Id
        LEFT JOIN _Interest i2 ON i2.InterestId = cfp.Interest2Id
        LEFT JOIN _Interest i3 ON i3.InterestId = cfp.Interest3Id
        INNER JOIN _RegisteredBy rb USING (RegisteredById)
        INNER JOIN _WatanRegion wr USING (WatanRegionId)
    WHERE
        ce.CandidateId = :id
    LIMIT 1
    -- without lookups
    SELECT
        ce.*,
        cfp.*
    FROM
        CandidateEx ce
        LEFT JOIN CandidateFullProfile cfp USING (CandidateId)
    WHERE
        ce.CandidateId = :id
    LIMIT 1

-- GET /candidate/{id}/shortlist
SELECT
    ce.ITSId,
    ce.FullName,
    ce.Age,
    ce.Location,
    ce.CandidateStatusId
FROM
    (SELECT
        cm.BoyCandidateId AS CandidateId,
        cm.GirlCandidateId AS OppositeCandidateId
    FROM
        CandidateMatchmaking cm
    WHERE
        cm.BoyPairStatusId IN (2,3)
    UNION SELECT
        cm.GirlCandidateId,
        cm.BoyCandidateId
    FROM
        CandidateMatchmaking cm
    WHERE
        cm.GirlPairStatusId IN (2,3)
    ) cm
    INNER JOIN CandidateEx ce ON ce.CandidateId = cm.OppositeCandidateId
WHERE
    cm.CandidateId = :id

-- GET /candidate/{id}/notes
    SELECT
        cpn.Note
    FROM
        CandidatePrivateNote cpn
    WHERE
        cpn.CandidateId = :id
    ORDER BY
        cpn.LastUpdated DESC

-- POST /candidate/{id}/notes?{notes}
    INSERT INTO
        CandidatePrivateNote cpn
    VALUES
        (NULL,:id,:notes,CURDATE())

-- PUT /candidate/{candidateid}/notes/{noteid}?{notes}
    UPDATE
        CandidatePrivateNote cpn
    SET
        cpn.Note = :notes,
        cpn.LastUpdated = CURDATE()
    WHERE
        cpn.CandidatePrivateNoteId = :noteid
        AND cpn.CandidateId = :candidateid
    LIMIT 1

-- DELETE /candidate/{candidateid}/notes/{noteid}
    DELETE FROM
        CandidatePrivateNote cpn
    WHERE
        cpn.CandidatePrivateNoteId = noteid
        AND cpn.CandidateId = candidateid
    LIMIT 1

-- GET /lookup/{table}/{partialname}
    -- jamaat
    SELECT
        j.JamaatId,
        j.Jamaat
    FROM
        _Jamaat j
    WHERE
        j.IsSpecial = FALSE
        AND j.Jamaat LIKE CONCAT('%',:partialname,'%')
    ORDER BY
        j.Jamaat
    LIMIT 10
    -- country
    SELECT
        c.CountryId,
        c.Country
    FROM
        _Country c
    ORDER BY
        c.Country
    -- place
    SELECT
        p.PlaceId,
        p.Place
    FROM
        _Place p
    WHERE
        p.Place LIKE CONCAT('%',:partialname,'%')
    ORDER BY
        p.Place
    LIMIT 10

-- GET /lookup/announcements
    SELECT
        a.AnnouncementId,
        a.Announcement
    FROM
        _Announcement a
    WHERE
        a.DisplayUpto >= CURDATE()
    ORDER BY
        a.Announcement

-- GET /photo/{itsid}