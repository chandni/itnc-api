<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CResponse {

    private $data = null;

    /**
     * Constructor
     * We will initialze the data for dummy response
     */
    public function __construct() {
        return $this->data = array("status" => 1, "error" => '', "data" => array());
    }

    public function setData($nStatus, $strError, $arrData) {
        $arrData = empty($arrData) ? (object)[] : $arrData;
        $this->data = array("status" => $nStatus, "error" => $strError, "data" => $arrData);
    }

    public function setStatus($nStatus) {
        $this->data["status"] = $nStatus;
    }

    public function setError($strError) {
        $this->data["error"] = $strError;
    }

    public function setPages($pages) {
        $this->data["pages"] = $this->array_filter_recursive($pages);
    }

    public function setResponseData($data) {
        $data = empty($data) ? (object)[] : $data;
        $this->data["data"] = $this->array_filter_recursive($data);
    }

    public function getData() {
        return $this->array_filter_recursive($this->$data);
    }

    public function getJson() {
        header("Content-Type:application/json");
        return json_encode($this->data);
    }

    public function array_filter_recursive($input) {
        if (is_array($input)) {
            foreach ($input as &$value) {
                if (is_array($value)) {
                    $value = $this->array_filter_recursive($value);
                }
            }
        }
        if (empty($input) && !is_array($input)) {
            $input = null;
            return ($input);
        } else {
            return ($input);
        }
    }

}
